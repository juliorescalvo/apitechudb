package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("apitechu/v2")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        final List<ProductModel> response = productService.findAll();
        return new ResponseEntity<>(response, (response != null && !response.isEmpty()) ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }

    @GetMapping("/products/{id-product}")
    public ResponseEntity<ProductModel> getProduct(@PathVariable("id-product") String idProduct) {
        final Optional<ProductModel> response = productService.findById(idProduct);
        return new ResponseEntity<>(response.orElse(null), response.orElse(null) != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> createProduct(@RequestBody ProductModel productModel) {
        return new ResponseEntity<>(productService.save(productModel), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id-product}")
    public ResponseEntity<ProductModel> updateProduct(@PathVariable("id-product") String idProduct, @RequestBody ProductModel productModel) {
        final ProductModel response = productService.update(idProduct, productModel);
        return new ResponseEntity<>(response, response != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);    }

    @DeleteMapping("/products/{id-product}")
    public ResponseEntity<String> deleteProduct(@PathVariable("id-product") String idProduct) {
        boolean response = productService.delete(idProduct);
        return new ResponseEntity<>(response ? "Producto borrado" : "Producto no borrado", response ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
