package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        final List<PurchaseModel> response = purchaseService.findAll();
        return new ResponseEntity<>(response, (response != null && !response.isEmpty()) ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        PurchaseServiceResponse result = purchaseService.save(purchase);
        return new ResponseEntity<>(result, result.getHttpStatusCode());
    }

}
