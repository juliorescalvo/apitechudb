package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("apitechu/v2")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(required = false) String orderBy) {
        final List<UserModel> response = userService.findAll(orderBy);
        return new ResponseEntity<>(response, (response != null && !response.isEmpty()) ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }

    @GetMapping("/users/{id-user}")
    public ResponseEntity<UserModel> getUser(@PathVariable("id-user") String idUser) {
        final Optional<UserModel> response = userService.findById(idUser);
        return new ResponseEntity<>(response.orElse(null), response.orElse(null) != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> createUser(@RequestBody UserModel userModel) {
        return new ResponseEntity<>(userService.save(userModel), HttpStatus.CREATED);
    }

    @PutMapping("/users/{id-user}")
    public ResponseEntity<UserModel> updateUser(@PathVariable("id-user") String idUser, @RequestBody UserModel userModel) {
        final UserModel response = userService.update(idUser, userModel);
        return new ResponseEntity<>(response, response != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id-user}")
    public ResponseEntity<String> deleteUser(@PathVariable("id-user") String idUser) {
        boolean response = userService.delete(idUser);
        return new ResponseEntity<>(response ? "Usuario borrado" : "Usuario no borrado", response ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
