package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<ProductModel> findAll() {
        return productRepository.findAll();
    }

    public ProductModel save(ProductModel productModel) {
        return productRepository.save(productModel);
    }

    public Optional<ProductModel> findById(String idProduct) {
        return productRepository.findById(idProduct);
    }

    public ProductModel update(String idProduct, ProductModel productModel) {
        productModel.setId(idProduct);
        if (this.findById(idProduct).isPresent()) {
            return productRepository.save(productModel);
        }
        return null;
    }

    public boolean delete(String idProduct) {
        if (this.findById(idProduct).isPresent()) {
            productRepository.deleteById(idProduct);
            return true;
        }
        return false;
    }

}
