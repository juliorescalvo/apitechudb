package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    public List<PurchaseModel> findAll() {
        return purchaseRepository.findAll();
    }

    public PurchaseServiceResponse save(PurchaseModel purchase) {
        final PurchaseServiceResponse result = new PurchaseServiceResponse();
        if (userService.findById(purchase.getUserId()).isEmpty()) {
            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }
        if (purchaseRepository.findById(purchase.getId()).isPresent()) {
            result.setMsg("Ya hay una compra con esa id");
            result.setHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }
        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (productService.findById(purchaseItem.getKey()).isEmpty()) {
                result.setMsg("El producto con la id " + purchaseItem.getKey()
                        + " no se encuentra en el sistema");
                result.setHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
            } else {
                amount +=
                        (productService.findById(purchaseItem.getKey()).get().getPrice()
                                * purchaseItem.getValue());
            }
        }
        purchase.setAmount(amount);
        result.setPurchase(purchaseRepository.save(purchase));
        result.setMsg("Compra añadida correctamente");
        result.setHttpStatusCode(HttpStatus.CREATED);

        return result;
    }

}
