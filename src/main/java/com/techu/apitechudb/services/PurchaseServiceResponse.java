package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.http.HttpStatus;

public class PurchaseServiceResponse {

    private String msg;
    private HttpStatus httpStatusCode;
    private PurchaseModel purchase;

    public PurchaseServiceResponse() {
    }

    public PurchaseServiceResponse(String msg, HttpStatus httpStatusCode, PurchaseModel purchase) {
        this.msg = msg;
        this.httpStatusCode = httpStatusCode;
        this.purchase = purchase;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public HttpStatus getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(HttpStatus httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public PurchaseModel getPurchase() {
        return purchase;
    }

    public void setPurchase(PurchaseModel purchase) {
        this.purchase = purchase;
    }

}
