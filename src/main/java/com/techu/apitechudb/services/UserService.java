package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<UserModel> findAll(String orderBy) {
        if (orderBy != null && !orderBy.equals("")) {
            return userRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy));
        }
        return userRepository.findAll();
    }

    public UserModel save(UserModel userModel) {
        return userRepository.save(userModel);
    }

    public Optional<UserModel> findById(String idUser) {
        return userRepository.findById(idUser);
    }

    public UserModel update(String idUser, UserModel userModel) {
        userModel.setId(idUser);
        if (this.findById(idUser).isPresent()) {
            return userRepository.save(userModel);
        }
        return null;
    }

    public boolean delete(String idUser) {
        if (this.findById(idUser).isPresent()) {
            userRepository.deleteById(idUser);
            return true;
        }
        return false;
    }

}
